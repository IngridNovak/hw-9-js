"use strict";

//1.Основний метод - document.createElement(someTag).
//2.Перший параметр - спосіб вставки стосовно опорного елемента. Може бути beforeBegin(перед опорним елементом),
//afterEnd(після опорного елемента), afterBegin(на початку опорного елементу),beforeEnd(в кінець).
//3.Видалити елемент можна за допомогою remove().

function createList(array, parent = document.body) {
    let html = '<ul>';

    array.forEach(function (item, i, arr) {
        if (item instanceof Array) {
            let div = document.createElement('div');
            item = createList(item, div);

        }
        html += '<li>' + item + '</li>';

    });
    html += '</ul>'
    return parent.innerHTML = parent.innerHTML + html;

}

let timer;
let x = 3;
countdown();

function countdown() {
    document.getElementById('time').innerHTML = x;
    x--;
    if (x < 0) {
        clearTimeout(timer);
    } else {
        timer = setTimeout(countdown, 1000);
    }
}

setTimeout(function () {
    clearInterval(countdown);
    document.body.replaceChildren();
}, 3000,);


createList(["1", "2", "3", [13, 12], "sea", "user", 23]);
